import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import PokemonRoster from './PokemonRoster'
// import Pokemon from './Pokemon'


class Roster extends Component {
  render() {
    return(
      <Switch>
          <Route exact path="/roster" component={PokemonRoster}></Route>
          {/* Couldnt finish in the given time the pokemon individual data routing */}
          {/* <Route path="/roster/:name" component={Pokemon}></Route> */}
      </Switch>
    )
  }
}

export default Roster
