import React, { Component } from 'react';
import {Link} from 'react-router-dom';

// Header generates links for navigation between routes.
class Header extends Component {
    render() {
        return(
            <header>
                <nav>
                    <ul>
                    <li><Link to='/'>Home</Link></li>
                    <li><Link to='/roster'>Pokemon Roster</Link></li>
                    </ul>
                </nav>
            </header>
        )

    }
}

export default Header