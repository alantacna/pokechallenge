import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import Home from './Home'
import Roster from './Roster'
import './style.css';


class Main extends Component {
  render() {
    return (
      <div className="main">
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/roster' component={Roster}/>
        </Switch>
      </div>  
    )
  }
}

export default Main