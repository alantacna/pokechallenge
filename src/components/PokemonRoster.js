import React, {Component} from 'react'
import {Link} from 'react-router-dom';
import axios from 'axios'
import './style.css'; 

class PokemonRoster extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pokemons: [],
            isLoading: false,
            error: null,
        }        
        
        this.fetchPokemons = this.fetchPokemons.bind(this)
    }
    componentDidMount() {
        this.fetchPokemons()        
    }
    fetchPokemons() {
        this.setState({isLoading: true})
        axios.get('https://pokeapi.co/api/v2/pokemon/')
            .then(response => 
                this.setState({
                    pokemons: response.data.results,
                    isLoading: false
                }))
            .catch(error => this.setState({
                error,
                isLoading: false
            }));
    }

    render() {        
        const {pokemons, isLoading, error} = this.state
        
        if(isLoading) {
            return <p>Loading...</p>
        }
        return (
            <ul className="grid"> 
                    {pokemons.map(pokemon => 
                        <li key={pokemon.name} className="pokeContainer">
                            <Link to={`/roster/${pokemon.url}`} className="clickMe">{pokemon.name}</Link>
                        </li>
                    )}
            </ul>
        )
    }
}

export default PokemonRoster
